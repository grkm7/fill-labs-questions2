package main 

import "fmt"

func main(){
	recursive(9)
}

func recursive(input int){   /// Function repaet itself by divide input into 2 until input reach 1 .
	if input==1 {
		return
	}

	recursive(input/2)
	fmt.Println(input)	

}
